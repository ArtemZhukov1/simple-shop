import React, { useEffect } from "react";
import { Link } from "react-router-dom";
// import {
//   faTimes,
//   faPlus,
//   faMinus,
// } from "@fortawesome/free-solid-svg-icons";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../style.css";
import { useDispatch, useSelector } from "react-redux";
import {
  productQtyDecrement,
  productQtyIncrement,
  removeProductFromCart,
} from "../redux/actions";
import { AppState } from "./typedef";


const Cart = () => {
  const dispatch = useDispatch();
  const state = useSelector((state: AppState) => state.cartReducer);
  useEffect(() => {}, [state]);
  //@ts-ignore
  const total = state.reduce(
    //@ts-ignore
    (t, { price, productQty }) => t + productQty * price,
    0
  );

  return (
    <section className="page-section color">
      <div className="container full-height min-height">
        <div className="row">
          <div className="col-md-12 text-center">
            <h2 className="block-header header-cart ">Your Cart</h2>
          </div>

          {state.length === 0 ? (
            <div className="col-md-12 text-center">Your catr is empty</div>
          ) : (
            state.map((i) => {
              return (
                <div
                  className={
                    state.length === 1 ? `col-md-6 offset-md-3` : `col-md-6`
                  }
                  key={i.id}
                >
                  <div className="wrapper-cart">
                    <div className="remove-product">
                      <button
                        onClick={() => dispatch(removeProductFromCart(i.id))}
                      >
                        X
                      </button>
                    </div>
                    <div className="image">
                      <img alt="" className="img-fluid" src={`${i.image}`} />
                    </div>
                    <h4 className="text-center ">{i.name}</h4>
                    <div className="price-unit ">${i.price}</div>
                    <div style={{ textAlign: "center", margin: "40px 0px" }}>
                      <span
                        className="counter"
                        onClick={() =>
                          dispatch(
                            productQtyDecrement({
                              id: i.id,
                              qty: i.productQty,
                            })
                          )
                        }
                      >
                        -
                      </span>
                      <span className="input">{i.productQty}</span>
                      <span
                        className="counter"
                        onClick={() =>
                          dispatch(
                            productQtyIncrement({
                              id: i.id,
                              qty: i.productQty,
                            })
                          )
                        }
                      >
                        +
                      </span>
                    </div>
                    <div className="subtotal">${i.price * i.productQty}</div>
                  </div>
                </div>
              );
            })
          )}

          <div className="col-md-12">
            <div className="shopping-cart">
              <div className="row">
                <div className="col-md-4">
                  <Link
                    to="/"
                    className="btn btn-theme btn-theme-dark btn-block"
                  >
                    Back Store
                  </Link>
                </div>
                <div className="col-md-4 text-center total-amount">
                  <span>Total:</span>
                  <span className="">${total}</span>
                </div>
                <div className="col-md-4">
                  <Link
                    to="/"
                    className="btn btn-theme btn-theme-dark btn-block"
                  >
                    Checkout
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Cart;
