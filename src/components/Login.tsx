import React, { useCallback, useContext } from "react";
import { Redirect } from "react-router-dom";
import { AuthContext } from "../Auth";
import { auth } from "../firebaseConfig";

const Login: React.FC<any> = ({ history }) => {
  const handleLogin = useCallback(
    async (event) => {
      event.preventDefault();
      const { email, password } = event.target.elements;
      console.log(email.value, password.value);
      try {
        await auth.signInWithEmailAndPassword(email.value, password.value);
        history.push("/admin");
      } catch (error) {
        alert(error);
      }
    },
    [history]
  );

  const { currentUser } = useContext(AuthContext);
  console.log(currentUser)
  if (currentUser) {
    return <Redirect to="/" />;
  }
  return (
    <div className="login">
      <div className="wrapper fadeInDown">
        <div id="formContent">
          <div className="fadeIn first">
            <h3 className="pt-3">Please sign in</h3>
          </div>

          <form onSubmit={handleLogin}>
            <input
              type="text"
              className="fadeIn second login-input"
              name="email"
              placeholder="email"
            />
            <input
              type="password"
              className="fadeIn third login-input"
              name="password"
              placeholder="password"
            />
            <input type="submit" className="fadeIn fourth" value="Log In" />
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
