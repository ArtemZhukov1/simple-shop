import React, { useState } from "react";
import { Link } from "react-router-dom";
import "../style.css";
import { useSelector } from "react-redux";
import { AppState, CategoryType, ProductType } from "./typedef";

const ProductList = () => {
  const products = useSelector(
    (state: AppState) => state.firebaseProductReducers
  );
  const categories = useSelector(
    (state: AppState) => state.firebaseCategoryReducers
  );
  //Sort by name and price
  const [sortByPrice, setSortByPrice] = useState("Product Name");
  const comparator = (a: any, b: any) => {
    if (sortByPrice === "price") {
      return a.price - b.price;
    }
    if (sortByPrice === "-price") {
      return b.price - a.price;
    }
    if (sortByPrice === "name") {
      return a.name.localeCompare(b.name);
    }
  };
  // filter by category
  const [showMe, setShowMe] = useState("All Products");
  const show = (i: ProductType) => {
    if (showMe === "All Products") return true;
    return i.category === showMe;
  };

  return (
    <section className="page-section">
      <div className="container-fluid container-shop">
        <div className="row">
          <div className="col-md-12 col-lg-12 content" id="content">
            <div className="shop-sorting">
              <div className="d-flex">
                <div className="d-none d-sm-block">Sort by</div>
                <div>
                  <select
                    className="selectClass"
                    value={sortByPrice}
                    onChange={(e) => setSortByPrice(e.target.value)}
                  >
                    <option value="name">Product Name</option>
                    <option value="-price">HIGHEST PRICE</option>
                    <option value="price">LOWEST PRICE</option>
                  </select>
                </div>
              </div>
              <div className="d-flex">
                <div className="d-none d-sm-block">Show me</div>
                <div>
                  <select
                    value={showMe}
                    onChange={(e) => setShowMe(e.target.value)}
                    className=""
                  >
                    <option value="All Products">All Products</option>
                    {categories.map((i: CategoryType) => (
                      <option key={i.id} value={i.name}>
                        {i.name}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>
            <div className="row products grid">
              {products
                .filter(show)
                .sort(comparator)
                .map((product: ProductType, index: number) => {
                  return (
                    <div className="col-md-4 col-sm-6 " key={index}>
                      <Link to={`/details/${product.id}`}>
                        <div className="thumbnail no-border">
                          <div className="media">
                            <img
                              alt=""
                              src={`${product.image && product.image[0]}`}
                            />
                          </div>
                          <div className="caption text-center">
                            <h4 className="caption-title ">{product.name}</h4>
                            <div className="price ">${product.price}</div>
                          </div>
                        </div>
                      </Link>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ProductList;
