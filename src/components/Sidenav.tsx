import React from "react";
import { Link } from "react-router-dom";
// import {
//   faTimes
// } from "@fortawesome/free-solid-svg-icons";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../style.css";

type Props = {
  menu: boolean
  toggleMenu:() => void
}


const Sidenav: React.FC<Props> = ({ menu, toggleMenu }) => {
    return (
      <>
        <div
          className={menu ? "menu-overlay" : " d-none"}
          onClick={() => toggleMenu()}
        ></div>
        <div className={menu ? "main-menu" : " d-none"}>
          <div className="close-menu" onClick={() => toggleMenu()}>
            {/* <FontAwesomeIcon icon={faTimes} /> */}
            <i className="fa fa-times"></i>
          </div>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/" className="active">
                Shop
              </Link>
            </li>
            <li>
              <Link to="/">About</Link>
            </li>
            <li>
              <Link to="/">Contact</Link>
            </li>
          </ul>
  
          <div className="social-media">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="http://facebook.com"
            >
              <span>
                <i className="fa fa-facebook"></i>
              </span>
            </a>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.pinterest.com/"
            >
              <span>
                <i className="fa fa-pinterest"></i>
              </span>
            </a>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://plus.google.com"
            >
              <span>
                <i className="fa fa-google-plus"></i>
              </span>
            </a>
          </div>
        </div>
      </>
    );
  };


  export default Sidenav;
