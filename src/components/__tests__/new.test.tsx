import React from "react"
import {shallow} from "enzyme";
// import Header from "../Header";
import Admin from "../Admin"
import {Provider} from "react-redux";
import store from "../../redux/store";
import {mount} from 'enzyme';
import {MemoryRouter} from 'react-router';

describe('Test ', () => {
    // it('should be rednered header ', function () {
    //     const wrapper = shallow(<Header/>);

    //     expect(wrapper.find('.header-main').length).toBe(1);
    // });
    it('should show admin page', () => {
        const wrapper = mount(
            <Provider store={store}>
                <MemoryRouter initialEntries={['/admin']}>
                    <Admin/>
                </MemoryRouter>
            </Provider>
        );

        expect(wrapper.find(Admin)).toHaveLength(1);
    });
    it('should show form add vacancy when click `Добавить вакансию` ', () => {
        const wrapper = mount(
            <Provider store={store}>
                <MemoryRouter initialEntries={['/admin']}>
                    <Admin/>
                </MemoryRouter>
            </Provider>
        );
        wrapper.find('.add-vacancy').simulate('click');

        expect(wrapper.find('.add-new-vacancy-title').length).toBe(1);
    });
});


