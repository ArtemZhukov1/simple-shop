import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { addProductToCart } from "../redux/actions";
import { AppState, ProductType } from "./typedef";

const ProductDetail = () => {
  const { id }: any = useParams();
  const dispatch = useDispatch();
  const products = useSelector(
    (state: AppState) => state.firebaseProductReducers
  );
  const product = products.filter((i: ProductType) => i.id === +id)[0];
  const [mainImage, setMainImage] = useState(`${product.image[0]}`);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [size, setSize] = useState("");

  return (
    <section className="page-section">
      <div className="container-fluid">
        <div className="row product-single ">
          <div className="col-md-6 text-center  relative column-height">
            <div className="images-wrapper">
              <img alt="" className="imgdetail" src={mainImage} />
              <div className="row">
                <div className="hidden-xs col-sm-3"></div>
                {product.image.length === 1
                  ? null
                  : product.image.map((i: string, index: number) => (
                      <div className="col-4 col-sm-2 " key={index}>
                        <div className="img-product-detail">
                          <div onClick={() => setMainImage(`${i}`)}>
                            <img className="full-width" alt="" src={`${i}`} />
                          </div>
                        </div>
                      </div>
                    ))}
              </div>
            </div>
          </div>
          <div className="col-md-6  filters-shop column-height">
            <div className="product-name">
              <h2 className="product-title ">{product.name}</h2>
              <div className="product-price ">${product.price}</div>
            </div>
            <div className="wrapper-info">
              <div className="product-text ">{product.desc}</div>
              <div className="buttons relative">
                <div>
                  {product.variation !== undefined ? (
                    <span className="select-wrapper-size ">
                      <select
                        defaultValue="Select size"
                        className="form-control size-select"
                        onChange={(e) => setSize(e.target.value)}
                        name="size"
                      >
                        <option disabled>Select size</option>
                        {product.variation.map((i) => (
                          <option key={i} value={i}>
                            {i}
                          </option>
                        ))}
                      </select>
                    </span>
                  ) : null}
                  {product.qty === 0 ? (
                    <label className="out-stock-label ">Out Stock</label>
                  ) : (
                    <span className="input-group-btn">
                      <button
                        className="btn-theme btn-detail"
                        onClick={() => dispatch(addProductToCart(product))}
                      >
                        Add to cart
                      </button>
                    </span>
                  )}
                </div>
              </div>
              <div className="message fadein fadeout"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ProductDetail;
