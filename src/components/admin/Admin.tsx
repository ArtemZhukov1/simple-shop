import React from "react";
import { auth } from "../../firebaseConfig";
import CategoryTable from "./category/CategoryTable";
import ProductTable from "./product/ProductTable";


const Admin = () => {
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <button className="btn" onClick={()=>{auth.signOut()}}>LogOut</button>
            <ProductTable />
            <CategoryTable />
          </div>
        </div>
      </div>
    </>
  );
};

export default Admin;
