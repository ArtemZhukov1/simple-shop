import React, { useState } from "react";
import { Field, FieldArray, Formik } from "formik";
import { AppState, CategoryType, ProductType } from "../../typedef";
import { database, storage } from "../../../firebaseConfig";
import { useSelector } from "react-redux";

type Props = {id:number};

const update = (task: ProductType) => {
  const id = task.id;
  database.ref(`/data/${id}`).update({
    ...task,
  });
};

const EditProduct: React.FC<Props> = ({id}) => {
  const [showModal, setShowModal] = useState(false);

  const productByID: ProductType = useSelector(
    (state: AppState) => state.firebaseProductReducers
  ).filter((i: ProductType) => i.id === id)[0];
  const categories = useSelector(
    (state: AppState) => state.firebaseCategoryReducers
  );

  const [images, setImages] = useState(productByID.image);

  const onFileChange = (e: any) => {
    const uploadedFiles: any = Array.from(e.target.files);
    uploadedFiles.forEach(async (file: any) => {
      const storageRef = storage.ref();
      const fileRef = storageRef.child(file.name);
      await fileRef.put(file);
      const imgURL = await fileRef.getDownloadURL();
      //@ts-ignore
      setImages((prevState) => [...prevState, imgURL]);
    });
  };

  return (
    <>
      <button
        className="btn btn-primary add-vacancy"
        onClick={() => setShowModal(!showModal)}
      >
        &#9998;
      </button>

      {showModal ? (
        <div className="modal">
          <div className="modal-content">
            <h2 className="text-center edit-title">Edit Product</h2>
            <Formik
              initialValues={{
                category: productByID.category,
                desc: productByID.desc,
                id: productByID.id,
                image: productByID.image,
                name: productByID.name,
                price: productByID.price,
                qty: productByID.qty,
                variation: productByID.variation || [],
              }}
              onSubmit={(values) => {
                update({
                  category: values.category,
                  desc: values.desc,
                  id: productByID.id,
                  image: images,
                  name: values.name,
                  price: values.price,
                  qty: values.qty,
                  variation: values.variation,
                });
                setShowModal(!showModal);
              }}
            >
              {({ values, handleChange, handleSubmit }) => {
                return (
                  <form onSubmit={handleSubmit}>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">Name</label>
                      <div className="col-sm-10">
                        <input
                          id="name"
                          className="form-control input-border"
                          type="text"
                          value={values.name}
                          onChange={handleChange}
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">
                        Desrciption
                      </label>
                      <div className="col-sm-10">
                        <input
                          id="desc"
                          className="form-control input-border"
                          type="text"
                          value={values.desc}
                          onChange={handleChange}
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">Price</label>
                      <div className="col-sm-10">
                        <input
                          id="price"
                          className="form-control input-border"
                          type="number"
                          value={values.price}
                          onChange={handleChange}
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">
                        Category
                      </label>
                      <div className="col-sm-10">
                        <Field
                          as="select"
                          name="category"
                          className="form-control input-border"
                        >
                          {categories.map((i: CategoryType) => (
                            <option key={i.id} value={i.name}>
                              {i.name}
                            </option>
                          ))}
                        </Field>
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">
                        Quantity
                      </label>
                      <div className="col-sm-10">
                        <input
                          id="qty"
                          className="form-control input-border"
                          type="number"
                          value={values.qty}
                          onChange={handleChange}
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">
                        Variation
                      </label>
                      <div className="col-sm-10">
                        <FieldArray name="variation">
                          {({ remove, push }) => (
                            <div className="d-flex flex-wrap">
                              {values.variation.length > 0 &&
                                values.variation.map((_, index) => (
                                  <div className="mr-3" key={index}>
                                    <Field
                                      className="variation-input input-border"
                                      name={`variation[${index}]`}
                                    />
                                    <button
                                      type="button"
                                      className="delete-button"
                                      onClick={() => remove(index)}
                                    >
                                      &#10006;
                                    </button>
                                  </div>
                                ))}
                              <button
                                type="button"
                                className="add-button"
                                onClick={() => push("")}
                              >
                                &#10010;
                              </button>
                            </div>
                          )}
                        </FieldArray>
                      </div>
                    </div>

                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">Images</label>
                      <div className="col-sm-10">
                        {images &&
                          images.map((i: string, indxm: number) => (
                            <>
                              <img
                                width={"150px"}
                                height={"auto"}
                                key={i}
                                alt={i}
                                src={i}
                              />
                              <button
                                type="button"
                                className="delete-button"
                                onClick={() =>
                                  setImages(
                                    images.filter(
                                      (i: string, indx: number) => indx !== indxm
                                    )
                                  )
                                }
                              >
                                &#10006;
                              </button>
                            </>
                          ))}
                      </div>
                    </div>

                    <div className="mb-3">
                      <input type="file" multiple onChange={onFileChange} />
                    </div>
                    <div className="d-flex justify-content-center">
                      <button type="submit" className="btn btn-success mr-3">
                      Save
                    </button>
                    <button
                      onClick={() => setShowModal(!showModal)}
                      className="btn btn-danger"
                    >
                      Cancel
                    </button>
                    </div>
                    
                  </form>
                );
              }}
            </Formik>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default EditProduct;




