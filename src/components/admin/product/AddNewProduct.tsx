import { Field, FieldArray, Formik } from "formik";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { database, storage } from "../../../firebaseConfig";
import { AppState, CategoryType, ProductType } from "../../typedef";

export const addTask = (task: ProductType) => {
  const id = task.id;
  database.ref(`/data/${id}`).set({
    ...task,
    id,
  });
};

const AddNewProduct = () => {
  const [showModal, setShowModal] = useState(false);
  const [images, setImages] = useState([]);
  const productsLength: number = useSelector(
    (state: AppState) => state.firebaseProductReducers
  ).length;
  const categories = useSelector(
    (state: AppState) => state.firebaseCategoryReducers
  );
  // const [category, setCategory] = useState("");
  const onFileChange = (e: any) => {
    const uploadedFiles = Array.from(e.target.files);
    uploadedFiles.forEach(async (file: any) => {
      const storageRef = storage.ref();
      const fileRef = storageRef.child(file.name);
      await fileRef.put(file);
      const imgURL = await fileRef.getDownloadURL();
      //@ts-ignore
      setImages((prevState) => [...prevState, imgURL]);
    });
  };

  return (
    <>
      <button
        className="btn btn-primary"
        onClick={() => setShowModal(!showModal)}
      >
        Add New Product &#10010;
      </button>

      {showModal ? (
        <div className="modal">
          <div className="modal-content">
            <h2 className="text-center add-new-vacancy-title">
              Add new product
            </h2>
            <Formik
              initialValues={{
                category: "",
                desc: "",
                id: productsLength + 1,
                image: images,
                name: "",
                price: 0,
                qty: 0,
                variation: [],
              }}
              onSubmit={(values) => {
                addTask({
                  category: values.category,
                  desc: values.desc,
                  id: values.id,
                  image: images,
                  name: values.name,
                  price: values.price,
                  qty: values.qty,
                  variation: values.variation,
                });
                setShowModal(!showModal);
              }}
            >
              {({ values, handleChange, handleSubmit, initialValues }) => {
                return (
                  <form onSubmit={handleSubmit}>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">Name</label>
                      <div className="col-sm-10">
                        <input
                          id="name"
                          className="form-control input-border"
                          type="text"
                          value={values.name}
                          onChange={handleChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">
                        Desrciption
                      </label>
                      <div className="col-sm-10">
                        <input
                          id="desc"
                          className="form-control input-border"
                          type="text"
                          value={values.desc}
                          onChange={handleChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">Price</label>
                      <div className="col-sm-10">
                        <input
                          id="price"
                          className="form-control input-border"
                          type="number"
                          value={values.price}
                          onChange={handleChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">
                        Category
                      </label>
                      <div className="col-sm-10">
                        <Field
                          as="select"
                          name="category"
                          className="form-control input-border"
                          required
                        >
                          {categories.map((i: CategoryType) => (
                            <option key={i.id} value={i.name}>{i.name}</option>
                          ))}
                        </Field>
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">
                        Quantity
                      </label>
                      <div className="col-sm-10">
                        <input
                          id="qty"
                          className="form-control input-border"
                          type="number"
                          value={values.qty}
                          onChange={handleChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">
                        Variation
                      </label>
                      <div className="col-sm-10">
                        <FieldArray name="variation">
                          {({ remove, push }) => (
                            <div className="d-flex flex-wrap">
                              {values.variation.length > 0 &&
                                values.variation.map((_, index) => (
                                  <div className="mr-3">
                                    <Field
                                      className="variation-input input-border"
                                      key={index}
                                      name={`variation[${index}]`}
                                    />
                                    <button
                                      type="button"
                                      className="delete-button"
                                      onClick={() => remove(index)}
                                    >
                                      &#10006;
                                    </button>
                                  </div>
                                ))}
                              <button
                                type="button"
                                className="add-button"
                                onClick={() => push("")}
                              >
                                &#10010;
                              </button>
                            </div>
                          )}
                        </FieldArray>
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">Images</label>
                      <div className="col-sm-10">
                        <input
                          type="file"
                          id="image"
                          multiple
                          onChange={onFileChange}
                          required
                        />
                        {images &&
                          images.map((i: string, indxm: number) => (
                            <>
                              <img
                                width={"150px"}
                                height={"auto"}
                                key={i}
                                alt={i}
                                src={i}
                              />
                              <button
                                type="button"
                                className="delete-button"
                                onClick={() =>
                                  setImages(
                                    images.filter(
                                      (i: string, indx: number) => indx !== indxm
                                    )
                                  )
                                }
                              >
                                &#10006;
                              </button>
                            </>
                          ))}
                      </div>
                    </div>
                    <div className="d-flex justify-content-center">
                      <button type="submit" className="btn btn-success mr-3">
                        Add product
                      </button>
                      <button
                        className="btn btn-danger"
                        onClick={() => setShowModal(!showModal)}
                      >
                        Cancel
                      </button>
                    </div>
                  </form>
                );
              }}
            </Formik>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default AddNewProduct;


