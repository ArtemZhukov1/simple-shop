import React from "react";
import { useSelector } from "react-redux";
import { AppState, ProductType } from "../../typedef";
import { database } from "../../../firebaseConfig";
import EditProduct from "./EditProduct";
import AddNewProduct from "./AddNewProduct";

const removeTask = (id: number | string) => {
  database.ref(`/data/${id}`).remove();
};

const ProductTable = () => {
  const products: ProductType[] = useSelector(
    (state: AppState) => state.firebaseProductReducers
  );

  return (
    <>
      <div className="d-flex justify-content-between align-items-center">
        <h2>All Products</h2>
        <AddNewProduct />
      </div>
      <div className="table-responsive-sm table-border mb-5">
        <table className="table">
          <thead className="thead-dark">
            <tr className="text-center">
              <th scope="col">ID</th>
              <th scope="col">Name</th>
              <th scope="col">Price</th>
              <th scope="col">Quantity</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {products && products.map((item: ProductType, index: number) => (
              <tr className="text-center" key={index}>
                <th scope="row">{item.id}</th>
                <th>{item.name}</th>
                <th>${item.price}</th>
                <th>{item.qty}</th>
                <th>
                  <EditProduct id={item.id} />
                  <button
                    className="btn btn-danger"
                    onClick={() => removeTask(item.id)}
                  >
                    &#10006;
                  </button>
                </th>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default ProductTable;
