import React, { useState } from "react";
import { Formik } from "formik";
import { database } from "../../../firebaseConfig";

const update = (id: number, name: string) => {
  database.ref(`/category/${id}`).update({ name });
};

const EditCategory: React.FC<any> = ({ category }) => {
  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <button
        className="btn btn-primary add-vacancy"
        onClick={() => setShowModal(!showModal)}
      >
        &#9998;
      </button>

      {showModal ? (
        <div className="modal">
          <div className="modal-content">
            <h2 className="text-center edit-title">Edit Product</h2>
            <Formik
              initialValues={{
                category: category.name,
              }}
              onSubmit={(values) => {
                update(category.id, values.category);
                setShowModal(!showModal);
              }}
            >
              {({ values, handleChange, handleSubmit }) => {
                return (
                  <form onSubmit={handleSubmit}>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">
                        Category
                      </label>
                      <div className="col-sm-10">
                        <input
                          id="category"
                          className="form-control input-border"
                          type="text"
                          value={values.category}
                          onChange={handleChange}
                        />
                      </div>
                    </div>
                    <div className="d-flex justify-content-center">
                      <button type="submit" className="btn btn-success mr-3">
                        Save
                      </button>
                      <button
                        onClick={() => setShowModal(!showModal)}
                        className="btn btn-danger"
                      >
                        Cancel
                      </button>
                    </div>
                  </form>
                );
              }}
            </Formik>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default EditCategory;
