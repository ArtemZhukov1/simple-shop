import { Formik } from "formik";
import React, { useState } from "react";
import { database } from "../../../firebaseConfig";

export const addCategory = (category: string) => {
  const id = Math.floor((Math.random() * 1000) + 1);
  database.ref(`category/${id}`).set({id,name:category});
};

const AddNewCategory = () => {
  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <button
        className="btn btn-primary"
        onClick={() => setShowModal(!showModal)}
      >
        Add New Category &#10010;
      </button>
      {showModal ? (
        <div className="modal">
          <div className="modal-content">
            <h2 className="text-center add-new-vacancy-title">
              Add New Category
            </h2>
            <Formik
              initialValues={{
                category: "",
              }}
              onSubmit={(values) => {
                console.log(values);
                addCategory( values.category);
                setShowModal(!showModal);
              }}
            >
              {({ values, handleChange, handleSubmit }) => {
                return (
                  <form onSubmit={handleSubmit}>
                    <div className="form-group row">
                      <label className="col-sm-2 col-form-label">
                        Category
                      </label>
                      <div className="col-sm-10">
                        <input
                          id="category"
                          className="form-control input-border"
                          type="text"
                          value={values.category}
                          onChange={handleChange}
                          required
                        />
                      </div>
                    </div>

                    <div className="d-flex justify-content-center">
                      <button type="submit" className="btn btn-success mr-3">
                        Add category
                      </button>
                      <button
                        className="btn btn-danger"
                        onClick={() => setShowModal(!showModal)}
                      >
                        Cancel
                      </button>
                    </div>
                  </form>
                );
              }}
            </Formik>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default AddNewCategory;
