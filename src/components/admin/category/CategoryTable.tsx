import React from "react";
import { useSelector } from "react-redux";
import { database } from "../../../firebaseConfig";
import { AppState, CategoryType } from "../../typedef";
import AddNewCategory from "./AddNewCategory";
import EditCategory from "./EditCategory";


const removeTask = (id: number | string) => {
  database.ref(`/category/${id}`).remove();
};


const CategoryTable = () => {
  const categories = useSelector(
    (state: AppState) => state.firebaseCategoryReducers
  );

  return (
    <>
      <div className="d-flex justify-content-between align-items-center">
        <h2>Categories</h2>
        <AddNewCategory />
      </div>
      <div className="table-responsive-sm table-border">
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {categories.map((item: CategoryType) => (
              <tr key={item.id}>
                <th>{item.name}</th>
                <th>
                  <EditCategory category={item} />
                  <button
                    className="btn btn-danger"
                    onClick={() => removeTask(item.id)}
                  >
                    &#10006;
                  </button>
                </th>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default CategoryTable;
