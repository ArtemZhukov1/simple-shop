export type CartType = {
    id:number
    name:string
    price:number
    image: string,
    productQty: number,
}
export type ProductType = {
    category: string
    desc: string
    id: number
    image: string[]
    name: string
    price: number
    qty:number
    variation:number[]
}

export type CategoryType = {
    id:number
    name:string
}

export interface AppState {
    cartReducer: CartType[],
    firebaseProductReducers: ProductType[],
    firebaseCategoryReducers: CategoryType[]
}