import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { AppState } from "./typedef";
  
type Props = {
  toggleMenu:() => void
}

const Header: React.FC<Props> = ({ toggleMenu }) => {
  const state = useSelector((state: AppState) => state.cartReducer);
  const total =  state.reduce(
      //@ts-ignore
    (t, { price, productQty }) => t + productQty * price,
    0
  );

  return (
    <header className="header ">
      <div className="header-wrapper top-bar">
        <div className="container-fluid">
          <div className="row justify-content-around align-items-center">
            <div className="">
              <div className="menu-icon">
                <button className='menu-btn' onClick={() => toggleMenu()}>
                  <span className="hidden-xs">Menu</span>
                </button>
              </div>
            </div>
            <div className="">
              <div className="header-logo text-center">
                <Link to="/" className="logo-text">
                  Simple Shop
                </Link>
              </div>
            </div>
            <div className="">
              <div className="header-cart">
                <div className="cart-wrapper">
                  <Link to="/cart" className="d-flex">
                    <div className="cart ">${total}.00</div>
                    <div className="d-none d-sm-block">Cart</div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
