import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "../style.css";
import Cart from "./Cart";
import Sidenav from "./Sidenav";
import Header from "./Header";
import ProductDetail from "./ProductDetail";
import ProductList from "./ProductList";


const Home = () => {
  const [menu, setMenu] = useState(false);
  const toggleMenu: () => void = () => setMenu(!menu);

  return (
    <Router>
      <Header toggleMenu={toggleMenu} />
      <Sidenav menu={menu} toggleMenu={toggleMenu} />
      <Switch>
        <Route exact path="/" component={ProductList} />
        <Route path="/details/:id" component={() => <ProductDetail />} />
        <Route path="/cart" component={() => <Cart />} />
      </Switch>
    </Router>
  );
};

export default Home;