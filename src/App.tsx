import React, { useEffect } from "react";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import { useDispatch } from "react-redux";
import {
  getCategoryThunk,
  getTasksThunk,
  watchCategoryAddedEvent,
  watchCategoryRemovedEvent,
  watchCategoryUpdateEvent,
  watchTaskAddedEvent,
  watchTaskRemovedEvent,
  watchTaskUpdateEvent,
} from "./redux/actions";
import Admin from "./components/admin/Admin";
import Home from "./components/Home";
import Login from "./components/Login";
import { AuthProvider } from "./Auth";
import PrivateRoute from "./PrivateRoute";


const App: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCategoryThunk());
    dispatch(watchCategoryAddedEvent());
    dispatch(watchCategoryRemovedEvent());
    dispatch(watchCategoryUpdateEvent());

    dispatch(getTasksThunk());
    dispatch(watchTaskAddedEvent());
    dispatch(watchTaskRemovedEvent());
    dispatch(watchTaskUpdateEvent());
  }, [dispatch]);
  

  return (
    <AuthProvider>
      <Router>
        <Switch>
          <Route exact path="/" component={() => <Home />} />
          <PrivateRoute path="/admin" component={() => <Admin />} />
          <Route exact path="/login" component={() => <Login />} />
          {/* <Route path="/cart" component={() => <Cart />} /> */}

          <Route path="*">404 Not found</Route>
        </Switch>
      </Router>
    </AuthProvider>
  );
};

export default App;
