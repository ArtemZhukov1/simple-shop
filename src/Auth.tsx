import React, { createContext, useEffect, useState } from "react";
import { auth } from "./firebaseConfig";

//@ts-ignore
export const AuthContext = createContext();
//@ts-ignore
export const AuthProvider = ({ children }) => {
  const [currentUser, setCurrentUser] = useState(null);
  const [pending, setPending] = useState(true);

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      //@ts-ignore
      setCurrentUser(user);
      setPending(false);
    });
  }, []);

  if (pending) {
    return (
      <div className="d-flex justify-content-center align-items-center">
        <img src="img/load.gif" alt="#" />
      </div>
    );
  }

  return (
    <AuthContext.Provider
      value={{
        currentUser,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
