import { database } from "../firebaseConfig";
import { ApiDispatch } from "./store";
import { CategoryType, ProductType } from "../components/typedef";
import { createAction } from "@reduxjs/toolkit";

export const addProductToCart = createAction<ProductType>(
  "ADD_PRODUCT_TO_CART"
);
export const removeProductFromCart = createAction<number>(
  "REMOVE_PRODUCT_FROM_CART"
);
export const productQtyDecrement = createAction<{ id: number; qty: number }>(
  "PRODUCT_QTY_DECREMENT"
);
export const productQtyIncrement = createAction<{ id: number; qty: number }>(
  "PRODUCT_QTY_INCREMENT"
);

export const getCategory = createAction<CategoryType[]>("GET_CATEGORY");
export const addCategory = createAction<CategoryType>("ADD_CATEGORY");
export const removeCategory = createAction<CategoryType>("REMOVE_CATEGORY");
export const updateCategory = createAction<CategoryType>("UPDATE_CATEGORY");

export const getVacancy = createAction<ProductType[]>("GET_VACANCY");
export const addVacancy = createAction<ProductType>("ADD_VACANCY");
export const removeVacancy = createAction<ProductType>("REMOVE_VACANCY");
export const updateVacancy = createAction<ProductType>("UPDATE_VACANCY");

export function getCategoryThunk() {
  return (dispatch: ApiDispatch) => {
    const ctegories: CategoryType[] = [];
    database
      .ref(`/category`)
      .once("value", (snap) => {
        snap.forEach((data) => {
          let category = data.val();
          ctegories.push(category);
        });
      })
      .then(() => dispatch(getCategory(ctegories)));
  };
}

export function watchCategoryAddedEvent() {
  return (dispatch: ApiDispatch) => {
    database.ref(`/category`).on("child_added", (snap) => {
      dispatch(addCategory(snap.val()));
    });
  };
}

export function watchCategoryRemovedEvent() {
  return (dispatch: ApiDispatch) => {
    database.ref(`/category`).on("child_removed", (snap) => {
      dispatch(removeCategory(snap.val()));
    });
  };
}

export function watchCategoryUpdateEvent() {
  return (dispatch: ApiDispatch) => {
    database.ref(`/category`).on("child_changed", (snap) => {
      dispatch(updateCategory(snap.val()));
    });
  };
}

export function watchTaskAddedEvent() {
  return (dispatch: ApiDispatch) => {
    database.ref(`/data`).on("child_added", (snap) => {
      dispatch(addVacancy(snap.val()));
    });
  };
}

export function watchTaskRemovedEvent() {
  return (dispatch: ApiDispatch) => {
    database.ref(`/data`).on("child_removed", (snap) => {
      dispatch(removeVacancy(snap.val()));
    });
  };
}

export function watchTaskUpdateEvent() {
  return (dispatch: ApiDispatch) => {
    database.ref(`/data`).on("child_changed", (snap) => {
      dispatch(updateVacancy(snap.val()));
    });
  };
}

export function getTasksThunk() {
  return (dispatch: ApiDispatch) => {
    const tasks: ProductType[] = [];
    database
      .ref(`/data`)
      .once("value", (snap) => {
        snap.forEach((data) => {
          let task = data.val();
          // console.log(task)
          tasks.push(task);
        });
      })
      .then(() => dispatch(getVacancy(tasks)));
  };
}
