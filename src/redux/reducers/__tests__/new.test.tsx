import firebaseProductReducers from "../firebase";
import {addVacancy, getVacancy, updateVacancy, removeVacancy} from "../../actions";

const vacancy = (id:number) => ({
    city: "string",
    country: "string",
    currency: "string",
    description: "string",
    id: id,
    name: "string",
    salary: "string"
});

describe('Test reducer',  () => {
    describe('when `getVacancy` action dispatched',  () => {
        it('should get vacancy',  () => {
            const result = firebaseProductReducers([],getVacancy([{
                city: "string",
                country: "string",
                currency: "string",
                description: "string",
                id: 1,
                name: "string",
                salary: "string"
            }]))

            expect(result).toMatchSnapshot()
        });
    });
    describe('when `addVacancy` action dispatched',  () => {
        it('should add vacancy to previous state',  () => {
            const state = vacancy(1);
            const secondVacancy = vacancy(2)
            const result = firebaseProductReducers([state] , addVacancy(secondVacancy))

            expect(result.length).toBe(2)
        });
    });
    describe('when `removeVacancy` action dispatched', () => {
        it('should remove vacancy from state', () => {
            const first = vacancy(1);
            const last = vacancy(2);
            const state = [first, last];
            const result = firebaseProductReducers(state, removeVacancy(first));

            expect(result).toEqual([last]);
        });
    });
    describe('when `updateVacancy` action dispatched',  () => {
        it('should update vacancy',  () => {
            const originalVacancy = vacancy(1)
            const updatedVacancy = {
                city: "string",
                country: "string",
                currency: "string",
                description: "string",
                id: 1,
                name: "Name",
                salary: "string"
            };
            const result = firebaseProductReducers([originalVacancy] , updateVacancy(updatedVacancy));

            expect(result[0].name).toBe("Name");
        });
    });
});