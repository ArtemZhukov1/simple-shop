import { createReducer } from "@reduxjs/toolkit";
import { CartType } from "../../components/typedef";
import {
  addProductToCart,
  removeProductFromCart,
  productQtyDecrement,
  productQtyIncrement,
} from "../actions";



export const cartReducer = createReducer<CartType[]>([], (builder) =>
  builder
    .addCase(addProductToCart, (state, action) => {
      let { id, name, price, image } = action.payload;
      let addedItem = {
        id,
        name,
        price,
        image: image[0],
        productQty: 1,
      };

      if (!!state.find((item: CartType) => item.id === id)) {
        state.map((i: CartType) => {
          if (i.id === id) {
            i.productQty++;
            return i;
          }
          return i;
        });
        return state;
      } else {
        return [...state, addedItem];
      }
    })
    .addCase(removeProductFromCart, (state, action) => {
      return state.filter((i: CartType) => i.id !== action.payload)
    })
    .addCase(productQtyDecrement, (state, action) => {
      if (+action.payload.qty === 0) {
        return state.filter((i: CartType) => i.id !== action.payload.id)
      } else {
        state.map((i: CartType) => {
          if (i.id === action.payload.id) {
            i.productQty--;
            return i;
          }
          return i;
        });
        return state;
      }
    })
    .addCase(productQtyIncrement, (state, action) => {
      localStorage.setItem('cart',JSON.stringify(state))
      state.map((i: CartType) => {
        if (i.id === action.payload.id) {
          i.productQty++;
          return i;
        }
        return i;
      });
      return state;
    })
);


export default cartReducer;
