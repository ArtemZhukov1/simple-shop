import { CategoryType, ProductType } from "../../components/typedef";
import { createReducer } from "@reduxjs/toolkit";
import {
  addCategory,
  addVacancy,
  getCategory,
  getVacancy,
  removeVacancy,
  updateVacancy,
} from "../actions";

export const firebaseProductReducers = createReducer<ProductType[]>([],(builder) =>
    builder
      .addCase(getVacancy, (state, action) => {
        return action.payload;
      })
      .addCase(addVacancy, (state, action) => {
        return [...state, action.payload];
      })
      .addCase(removeVacancy, (state, action) => {
        return state.filter((task) => task.id !== action.payload.id);
      })
      .addCase(updateVacancy, (state, action) => {
        return [
          ...state.filter((task) => task.id !== action.payload.id),
          action.payload,
        ];
      })
);

export const firebaseCategoryReducers = createReducer<CategoryType[]>(
  [],
  (builder) =>
    builder.addCase(getCategory, (state, action) => {
      return action.payload;
    })
  .addCase(addCategory, (state, action) => {
    return [...state, action.payload];
  })
  // .addCase(removeCategory, (state, action) => {
  //   return state.filter((task) => task.id !== action.payload.id);
  // })
  // .addCase(updateCategory, (state, action) => {
  //   return [
  //     ...state.filter((task) => task.id !== action.payload.id),
  //     action.payload,
  //   ];
  // })
);

// .addCase(getCategory, (state, action) => {
//   return action.payload;
// })
