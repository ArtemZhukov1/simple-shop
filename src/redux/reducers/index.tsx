import { combineReducers } from 'redux'
import {firebaseProductReducers, firebaseCategoryReducers} from "./firebase";
import cartReducer from "./cartReducer"

export default combineReducers({
  firebaseProductReducers,
  firebaseCategoryReducers,
  cartReducer
})

