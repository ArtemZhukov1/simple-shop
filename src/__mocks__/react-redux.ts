//@ts-ignore
const actual = jest.requireActual('react-redux');

const dispatch = jest.fn(action => Promise.resolve(action));

actual.useDispatch = jest.fn(() => dispatch);
actual.useSelector = jest.fn(fn => fn());

module.exports = actual;